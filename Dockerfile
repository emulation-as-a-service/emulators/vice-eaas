from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="vice-sdl"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
autoconf autotools-dev bison flex build-essential

add vice-sdl /vice-sdl
workdir vice-sdl

run autoreconf -i
run ./configure  --prefix=/usr --without-x --enable-ethernet --enable-sdlui --with-sdlsound --enable-parsid --with-midas --without-xaw3d --enable-arch=no --disable-no-pic
run make
run make install

add vice-data/opt /opt
add metadata /metadata
